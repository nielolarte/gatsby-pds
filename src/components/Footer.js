import React from 'react'
import styled from 'styled-components'

import rem from '../utils/rem'

const Wrapper = styled.div`
  min-height: ${rem(10)};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.7rem 0 0 0;
  background: rebeccapurple;
  color: white;
`

// const FooterLink = styled.a`
//   color: currentColor;
//   text-decoration: underline;
//   transition: opacity: 0.2s;
//   &:hover {
//     opacity: 0.8;
//   }
//   &:active {
//     opacity: 0.6;
//   }
// `

export default () => (
  <Wrapper>
    <p>
      © {new Date().getFullYear()} Alphabetization Pour Tous
    </p>
  </Wrapper>
)
