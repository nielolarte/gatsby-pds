import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'

import NavSeparator from './NavSeparator'

const Wrapper = styled.nav`
  display: flex;
  align-items: center;
  flex: 0 0 auto;
`

const NavLink = styled(Link)`
  flex: 0 0 auto;
  display: inline-block;
  line-height: 3.125rem;
  transition: opacity 0.2s, transform 0.2s;
  cursor: pointer;
  letter-spacing: 0.025rem;
  color: currentColor;
  &:hover,
  &:focus {
    opacity: 0.8;
  }
  &:active {
    transform: scale(0.95);
    opacity: 0.6;
  }
`

const NavItem = styled.div`
  flex: 0 0 auto;
  display: inline-block;
  line-height: 3.125rem;
  transition: opacity 0.2s, transform 0.2s;
  cursor: pointer;
  letter-spacing: 0.025rem;
  color: currentColor;
  &:hover,
  &:focus {
    opacity: 0.8;
    color: #13aff0;
    text-decoration: none;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
  }
  &:active {
    transform: scale(0.95);
    opacity: 0.6;
  }
`

const SecondaryNavbar = styled.div`
  display: none;
  position: absolute;
  background: rgba(102, 51, 153, 0.8);
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  &:hover {
    display: block;
  }
`

const SecondaryNavLink = styled(Link)`
  flex: 0 0 auto;
  display: block;
  line-height: 3.125rem;
  padding: 0 1rem 0 1rem;
  cursor: pointer;
  letter-spacing: 0.025rem;
  color: currentColor;
  &:hover,
  &:focus {
    opacity: 0.8;
  }
  &:active {
    transform: scale(0.95);
    opacity: 0.6;
  }
`

const WrapperHover = styled.div`
    & ${NavItem}:hover + ${SecondaryNavbar} {
      display: block;
    }
`

const NavLinks = () =>
  <Wrapper>
    <NavLink to="/">Home</NavLink>
    <NavSeparator />
    <span>
    <WrapperHover>
      <NavItem>Info Index</NavItem>
      <SecondaryNavbar>
        <SecondaryNavLink to="/come-and-visit">Come And Visit</SecondaryNavLink>
        <SecondaryNavLink to="/in-and-about-samara">In And About Samara</SecondaryNavLink>
        <SecondaryNavLink to="/more-about-prados-del-sol">More About Prados Del Sol</SecondaryNavLink>
        <SecondaryNavLink to="/where-to-eat-in-costa-rica">Where To Eat In Costa Rica</SecondaryNavLink>
      </SecondaryNavbar>
    </WrapperHover>
    </span>
    <NavSeparator />
    <NavLink to="book">Book</NavLink>
    <NavSeparator />
    <NavLink to="prados-rental-rates">Rental Rates</NavLink>
  </Wrapper>

export default NavLinks
