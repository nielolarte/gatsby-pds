import React from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'
import styled, { css } from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

import rem from '../../utils/rem'
import { navbarHeight } from '../../utils/sizes'
import { mobile } from '../../utils/media'
import MobileNavLinks from './MobileNavLinks'
import NavButton from './NavButton'

const Wrapper = styled.div`
  display: none;

  ${mobile(css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: ${rem(navbarHeight)};
  `)}
`

const SecondaryMenu = styled.div`
  position: absolute;
  top: 3.125rem;
  left: 0;
  right: 0;
  ${p => p.open ? css`
    height: 15rem;
  ` : css`
    height: 0;
    overflow-y: hidden;
  `}
  display: block;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: space-between;
  padding: 0 1.25rem;
  transition: height 0.1s;
  user-select: none;
  -webkit-overflow-scrolling: touch;
  background: rgba(102, 51, 153, 0.8);
  color: currentColor;
`

const LogoLink = styled(Link).attrs({
  to: '/',
  'aria-label': 'home',
})`
  display: inline-block;
  vertical-align: center;
  margin-left: 1.25rem;
  color: currentColor;
  font-size: large;
  font-weight: bold;
`

const IconWrapper = styled.div`
  transition: transform 0.1s;
  color: white;
  ${p => p.rotate && css`
    transform-origin: 50% 55%;
    transform: rotate(180deg);
  `}
`

const MobileNavbar = props => {
  const {
    isMobileNavFolded,
    onMobileNavToggle,
  } = props

  return (
    <StaticQuery query={graphql` query { site { siteMetadata { title } } } `} render={data => (
      <Wrapper>
        <LogoLink>
          <span>{data.site.siteMetadata.title}</span>
        </LogoLink>

        <Wrapper>
          <NavButton
            onClick={onMobileNavToggle}
            active={!isMobileNavFolded}
          >
            <IconWrapper rotate={!isMobileNavFolded}>
              <FontAwesomeIcon icon={faChevronDown} />
            </IconWrapper>
          </NavButton>
        </Wrapper>

        <SecondaryMenu open={!isMobileNavFolded}>
          <MobileNavLinks />
        </SecondaryMenu>
      </Wrapper>
    )} />
  )
}

export default MobileNavbar
