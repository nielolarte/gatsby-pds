import React from "react"
import { graphql } from "gatsby"
import unescape from "unescape"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PageTemplate = ({ data }) => (
  <Layout>
    <SEO
      title={unescape(data.wordpressPage.title)}
      description={data.wordpressPage.excerpt}
    />
    <div dangerouslySetInnerHTML={{ __html: data.wordpressPage.content }} />
  </Layout>
)
export default PageTemplate
export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      title
      excerpt
      content
    }
  }
`
