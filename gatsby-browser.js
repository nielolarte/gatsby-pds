/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import "./src/styles/home.css"
import "./src/styles/come-and-visit.css"
import "./src/styles/come-and-visit-sub-pages.css"
import "./src/styles/in-and-about-samara.css"
import "./src/styles/more-about-prados-del-sol.css"
import "./src/styles/book.css"
import "./src/styles/prados-rental-rates.css"
import "./src/styles/where-to-eat-in-costa-rica.css"
import "./src/styles/where-to-eat-in-costa-rica-sub-pages.css"
